#include <lynx.h>
#include <tgi.h>
#include <6502.h>
#include <joystick.h>
#include <stdlib.h>

extern void tgi_arialxy(int x, int y, char *msg);

typedef struct {
    SCB_REHV sprite;
    PENPAL_4;
} isprite_t;

void main()
{
    unsigned char joy;
    int j = 0;

    joy_install(&lynx_stdjoy);
    tgi_install(&lynx_160_102_16);
    tgi_init();
    CLI();
    while (1) {
        while (tgi_busy())
            ;
        joy = joy_read(JOY_1);
        if (JOY_BTN_UP(joy)) {
            j++;
        }
        if (JOY_BTN_DOWN(joy)) {
            j--;
        }
        tgi_clear();
        tgi_setcolor(COLOR_WHITE);
        tgi_bar(0, 0, 159, 101);
        tgi_setcolor(COLOR_DARKBROWN);
        tgi_arialxy(1, j+10, "The Arial font");
        tgi_arialxy(1, j+30, " !\"#$%&'()*+,-./");
        tgi_arialxy(1, j+41, "0123456789:;<=>?");
        tgi_arialxy(1, j+52, "@ABCDEFGHIJKLMNO");
        tgi_arialxy(1, j+63, "PQRSTUVWXYZ[\\]^_");
        tgi_arialxy(1, j+74, "`abcdefghijklmno");
        tgi_arialxy(1, j+85, "pqrstuvwxyz{|}~");
        tgi_updatedisplay();
    
    }
}

