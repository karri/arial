#include <lynx.h>
#include <tgi.h>

static const unsigned char fonta[];

#include "Hexclam.c"
#include "Hquote.c"
#include "Hhash.c"
#include "Hdollar.c"
#include "Hpercent.c"
#include "Hampersand.c"
#include "Hsinglequote.c"
#include "Hleftparen.c"
#include "Hrightparen.c"
#include "Hstar.c"
#include "Hplus.c"
#include "Hcomma.c"
#include "Hminus.c"
#include "Hperiod.c"
#include "Hslash.c"
#include "H0.c"
#include "H1.c"
#include "H2.c"
#include "H3.c"
#include "H4.c"
#include "H5.c"
#include "H6.c"
#include "H7.c"
#include "H8.c"
#include "H9.c"
#include "Hcolon.c"
#include "Hsemicolon.c"
#include "Hlessthan.c"
#include "Hequal.c"
#include "Hgreaterthan.c"
#include "Hquestion.c"
#include "Hat.c"
#include "HA.c"
#include "HB.c"
#include "HC.c"
#include "HD.c"
#include "HE.c"
#include "HF.c"
#include "HG.c"
#include "HH.c"
#include "HI.c"
#include "HJ.c"
#include "HK.c"
#include "HL.c"
#include "HM.c"
#include "HN.c"
#include "HO.c"
#include "HP.c"
#include "HQ.c"
#include "HR.c"
#include "HS.c"
#include "HT.c"
#include "HU.c"
#include "HV.c"
#include "HW.c"
#include "HX.c"
#include "HY.c"
#include "HZ.c"
#include "Hleftsquarebracket.c"
#include "Hbackslash.c"
#include "Hrightsquarebracket.c"
#include "Hcaret.c"
#include "Hunderscore.c"
#include "Hgrave.c"
#include "Ha.c"
#include "Hb.c"
#include "Hc.c"
#include "Hd.c"
#include "He.c"
#include "Hf.c"
#include "Hg.c"
#include "Hh.c"
#include "Hi.c"
#include "Hj.c"
#include "Hk.c"
#include "Hl.c"
#include "Hm.c"
#include "Hn.c"
#include "Ho.c"
#include "Hp.c"
#include "Hq.c"
#include "Hr.c"
#include "Hs.c"
#include "Ht.c"
#include "Hu.c"
#include "Hv.c"
#include "Hw.c"
#include "Hx.c"
#include "Hy.c"
#include "Hz.c"
#include "Hleftcurlybracket.c"
#include "Hpipe.c"
#include "Hrightcurlybracket.c"
#include "Htilde.c"

typedef struct {
    const unsigned char *bitmap;
    unsigned char width;
} font_t;

static font_t arial[] = {
    {fontexclam, fontexclam_WIDTH},
    {fontquote, fontquote_WIDTH},
    {fonthash, fonthash_WIDTH},
    {fontdollar, fontdollar_WIDTH},
    {fontpercent, fontpercent_WIDTH},
    {fontampersand, fontampersand_WIDTH},
    {fontsinglequote, fontsinglequote_WIDTH},
    {fontleftparen, fontleftparen_WIDTH},
    {fontrightparen, fontrightparen_WIDTH},
    {fontstar, fontstar_WIDTH},
    {fontplus, fontplus_WIDTH},
    {fontcomma, fontcomma_WIDTH},
    {fontminus, fontminus_WIDTH},
    {fontperiod, fontperiod_WIDTH},
    {fontslash, fontslash_WIDTH},
    {font0, font0_WIDTH},
    {font1, font1_WIDTH},
    {font2, font2_WIDTH},
    {font3, font3_WIDTH},
    {font4, font4_WIDTH},
    {font5, font5_WIDTH},
    {font6, font6_WIDTH},
    {font7, font7_WIDTH},
    {font8, font8_WIDTH},
    {font9, font9_WIDTH},
    {fontcolon, fontcolon_WIDTH},
    {fontsemicolon, fontsemicolon_WIDTH},
    {fontlessthan, fontlessthan_WIDTH},
    {fontequal, fontequal_WIDTH},
    {fontgreaterthan, fontgreaterthan_WIDTH},
    {fontquestion, fontquestion_WIDTH},
    {fontat, fontat_WIDTH},
    {fontA, fontA_WIDTH},
    {fontB, fontB_WIDTH},
    {fontC, fontC_WIDTH},
    {fontD, fontD_WIDTH},
    {fontE, fontE_WIDTH},
    {fontF, fontF_WIDTH},
    {fontG, fontG_WIDTH},
    {fontH, fontH_WIDTH},
    {fontI, fontI_WIDTH},
    {fontJ, fontJ_WIDTH},
    {fontK, fontK_WIDTH},
    {fontL, fontL_WIDTH},
    {fontM, fontM_WIDTH},
    {fontN, fontN_WIDTH},
    {fontO, fontO_WIDTH},
    {fontP, fontP_WIDTH},
    {fontQ, fontQ_WIDTH},
    {fontR, fontR_WIDTH},
    {fontS, fontS_WIDTH},
    {fontT, fontT_WIDTH},
    {fontU, fontU_WIDTH},
    {fontV, fontV_WIDTH},
    {fontW, fontW_WIDTH},
    {fontX, fontX_WIDTH},
    {fontY, fontY_WIDTH},
    {fontZ, fontZ_WIDTH},
    {fontleftsquarebracket, fontleftsquarebracket_WIDTH},
    {fontbackslash, fontbackslash_WIDTH},
    {fontrightsquarebracket, fontrightsquarebracket_WIDTH},
    {fontcaret, fontcaret_WIDTH},
    {fontunderscore, fontunderscore_WIDTH},
    {fontgrave, fontgrave_WIDTH},
    {fonta, fonta_WIDTH},
    {fontb, fontb_WIDTH},
    {fontc, fontc_WIDTH},
    {fontd, fontd_WIDTH},
    {fonte, fonte_WIDTH},
    {fontf, fontf_WIDTH},
    {fontg, fontg_WIDTH},
    {fonth, fonth_WIDTH},
    {fonti, fonti_WIDTH},
    {fontj, fontj_WIDTH},
    {fontk, fontk_WIDTH},
    {fontl, fontl_WIDTH},
    {fontm, fontm_WIDTH},
    {fontn, fontn_WIDTH},
    {fonto, fonto_WIDTH},
    {fontp, fontp_WIDTH},
    {fontq, fontq_WIDTH},
    {fontr, fontr_WIDTH},
    {fonts, fonts_WIDTH},
    {fontt, fontt_WIDTH},
    {fontu, fontu_WIDTH},
    {fontv, fontv_WIDTH},
    {fontw, fontw_WIDTH},
    {fontx, fontx_WIDTH},
    {fonty, fonty_WIDTH},
    {fontz, fontz_WIDTH},
    {fontleftcurlybracket, fontleftcurlybracket_WIDTH},
    {fontpipe, fontpipe_WIDTH},
    {fontrightcurlybracket, fontrightcurlybracket_WIDTH},
    {fonttilde, fonttilde_WIDTH},
};

typedef struct {
    SCB_REHV sprite;
    PENPAL_1;
} sprite_t;

static sprite_t ch = {
    {
        BPP_1 | TYPE_NORMAL,
        LITERAL | REHV,
        0x20,
        0, 0,
        1, 0, 256, 256
    },
    {COLOR_WHITE}
};

void tgi_arialxy(int x, int y, char *msg)
{
    if ((y < -10) || (y > 112)) return;
    ch.sprite.hpos = x;
    ch.sprite.vpos = y;
    ch.penpal[0] = tgi_getcolor();
    while (*msg) {
        char cha = *msg;
        if (cha == ' ') {
            ch.sprite.hpos += 4;
        }
        if ((cha > ' ') && (cha <= '~')) {
            unsigned char index = cha - '!';
            ch.sprite.data = (unsigned char *)arial[index].bitmap;
            tgi_sprite(&ch.sprite);
            ch.sprite.hpos += arial[index].width + 1;
        }
        ++msg;
    }
}

int tgi_ariallen(char *msg)
{
    int len = 0;
    while (*msg) {
        char cha = *msg;
        if (cha == ' ') {
            len += 4;
        }
        if ((cha > ' ') && (cha <= '~')) {
            unsigned char index = cha - '!';
            len += arial[index].width + 1;
        }
        ++msg;
    }
}

